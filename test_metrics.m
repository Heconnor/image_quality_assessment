clear all
% initial setting
mt1 = get_variable_via_load('data/phantom.mat');
mp1 = get_variable_via_load('data/recon_1.mat');
mp2 = get_variable_via_load('data/recon_2.mat');

% 64, 64, 64
im1 = mt1(:,:,32);
im2 = mp1(:,:,32);
im3 = mp2(:,:,32);

M = metrics;
% snr  越大越好
SNR12 = M.calc_snr(im1, im2);
SNR13 = M.calc_snr(im1, im3);

% psnr  越大，失真越小
PSNR12 = M.calc_psnr(im1, im2);
PSNR13 = M.calc_psnr(im1, im3);

% rmse  越小越好   均方根误差 RMSE 受异常值的影响更大
RMSE12 = M.calc_rmse(im1, im2);
RMSE13 = M.calc_rmse(im1, im3);

% mae  越大，失真越小
MAE12 = M.calc_mae(im1, im2);
MAE13 = M.calc_mae(im1, im3);

% ssim
[ssim_map12, mssim12] = M.compute_map_ssim(im1, im2, 11, 1.5);
[ssim_map13, mssim13] = M.compute_map_ssim(im1, im3, 11, 1.5);

pmin = min(im1(:))*0.9;
pmax = max(im1(:))*1.1;
figure()
subplot(311)
imshow(im1, [pmin, pmax]);
title('Original image');
subplot(312)
imshow(im2, [pmin, pmax]);
title(sprintf('SNR: %.2f, PSNR: %.2f, RMSE: %.2f, MAE: %.2f', SNR12, PSNR12, RMSE12, MAE12));
subplot(313)
imshow(im3, [pmin, pmax]);
title(sprintf('SNR: %.2f, PSNR: %.2f, RMSE: %.2f, MAE: %.2f', SNR13, PSNR13, RMSE13, MAE13));


pmin = min([min(ssim_map12(:)), min(ssim_map13(:))]) * 0.9;
figure()
subplot(211)
imshow(ssim_map12,[pmin, 1]);
title(sprintf('MSSIM: %.3f', mssim12));
subplot(212)
imshow(ssim_map13,[pmin, 1]);
title(sprintf('MSSIM: %.3f', mssim13));

