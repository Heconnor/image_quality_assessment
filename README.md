# [Image Quality Assessment](https://gitee.com/Heconnor/image_quality_assessment)

![](https://img.shields.io/badge/python-v3.7-blue.svg)
![](https://img.shields.io/badge/matlab-2017a-blue.svg)
[![](https://img.shields.io/badge/license-MIT-green.svg)](https://gitee.com/Heconnor/image_quality_assessment/blob/master/LICENSE)
[![star](https://gitee.com/Heconnor/image_quality_assessment/badge/star.svg?theme=dark)](https://gitee.com/Heconnor/image_quality_assessment/stargazers)
[![fork](https://gitee.com/Heconnor/image_quality_assessment/badge/fork.svg?theme=dark)](https://gitee.com/Heconnor/image_quality_assessment/members)
<div align="center">
    <img src="./data/assessment.png" width="65%">
</div>


## Table of Contents
* [Brief description](#brief-description)
* [TODO](#todo)
* [Simulated Datasets](#simulated-datasets)
* [Requirements](#requirements)
* [Test the package](#test-the-package)
* [Contributors](#contributors)

## Brief description

> This repository contains various methods to assess the quality of an image
and to construct simulated dataset to test tomographic reconstruction algorithms.

## Requirements

[![h5py](https://img.shields.io/pypi/v/h5py.svg?label=h5py)](https://pypi.org/project/h5py/)
[![Pillow](https://img.shields.io/pypi/v/Pillow.svg?label=Pillow)](https://pypi.org/project/Pillow/)
[![scikit-image](https://img.shields.io/pypi/v/scikit-image.svg?color=orange&label=scikit-image)](https://pypi.org/project/scikit-image/)
[![scipy](https://img.shields.io/pypi/v/scipy.svg?color=orange&label=scipy)](https://pypi.org/project/scipy/)

## TODO
> The following metrics are included ([Python](metrics.py) or [MATLAB](metrics.m)):

  - [x] Signal-to-Noise-Ratio (SNR).
  - [x] Peak-Signal-to-Noise-Ratio (PSNR).
  - [x] Root-Mean-Squared-Error (RMSE).
  - [x] Mean-Absolute-Error (MAE)
  - [x] Structural Similarity Index (SSIM).
  - [ ] Normalized Mutual Information (NMI).
  - [ ] Image Complexity.
  - [ ] Resolution analysis through Edge-Profile-Fitting (EPF).
  - [ ] Resolution analysis through Fourier Ring Correlation (FRC).

## Simulated Datasets
> The following routines to construct simulated datasets are included:

- Create a Shepp-Logan phantom.

- Create generic phantoms with analytical X-ray transform.

- Rescale image.

- Downsample sinogram.

- Add Gaussian or Poisson noise.

- Add Gaussian blurring.

## Test the package
Go inside the folder "data/" and unzip the test dataset: `unzip dataset.zip`.

Then, inside the folder "tests/" try to run one by one the test scripts.

When a plot is produced during the execution of a test, the script is halted until
the plot window is manually closed.

## Contributors
[![Heconnor](https://img.shields.io/badge/author-Heconnor-blue.svg)](https://gitee.com/Heconnor)
[![arcaduf](https://img.shields.io/badge/author-arcaduf-blue.svg)](https://github.com/arcaduf)
